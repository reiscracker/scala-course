package objsets

import org.scalatest.FunSuite


import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TweetSetSuite extends FunSuite {
  trait TestSets {
    val set1 = new Empty
    val set2 = set1.incl(new Tweet("a", "a body", 20))
    val set3 = set2.incl(new Tweet("b", "b body", 20))
    val c = new Tweet("c", "c body", 7)
    val d = new Tweet("d", "d body", 9)
    val set4c = set3.incl(c)
    val set4d = set3.incl(d)
    val set5 = set4c.incl(d)

    val mostRetweetedTestSet = set2 incl c incl d

    val googleTweet1 = new Tweet("Hannes", "an android tweet", retweets = 1)
    val googleTweet2 = new Tweet("Hannes", "an Android tweet", retweets = 1)
    val googleTweet3 = new Tweet("Hannes", "a galaxy tweet", retweets = 1)
    val googleTweet4 = new Tweet("Hannes", "a Galaxy tweet", retweets = 1)
    val googleTweet5 = new Tweet("Hannes", "A nexus tweet", retweets = 1)
    val googleTweet6 = new Tweet("Hannes", "A Nexus tweet", retweets = 1)
    val googleTweets = new Empty incl googleTweet1 incl googleTweet2 incl googleTweet3 incl googleTweet4 incl googleTweet5 incl googleTweet6
    val appleTweet1 = new Tweet("Hannes", "an ios tweet", retweets = 1)
    val appleTweet2 = new Tweet("Hannes", "an iOS tweet", retweets = 1)
    val appleTweet3 = new Tweet("Hannes", "a iphone tweet", retweets = 1)
    val appleTweet4 = new Tweet("Hannes", "a iPhone tweet", retweets = 1)
    val appleTweet5 = new Tweet("Hannes", "A ipad tweet", retweets = 1)
    val appleTweet6 = new Tweet("Hannes", "A iPad tweet", retweets = 1)
    val appleTweets = new Empty incl appleTweet1 incl appleTweet2 incl appleTweet3 incl appleTweet4 incl appleTweet5 incl appleTweet6
  }

  def asSet(tweets: TweetSet): Set[Tweet] = {
    var res = Set[Tweet]()
    tweets.foreach(res += _)
    res
  }

  def size(set: TweetSet): Int = asSet(set).size

  test("filter: on empty set") {
    new TestSets {
      assert(size(set1.filter(tw => tw.user == "a")) === 0)
    }
  }

  test("filter: a on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.user == "a")) === 1)
    }
  }

  test("filter: 20 on set5") {
    new TestSets {
      assert(size(set5.filter(tw => tw.retweets == 20)) === 2)
    }
  }

  test("union: set4c and set4d") {
    new TestSets {
      assert(size(set4c.union(set4d)) === 4)
    }
  }

  test("union: with empty set (1)") {
    new TestSets {
      assert(size(set5.union(set1)) === 4)
    }
  }

  test("union: with empty set (2)") {
    new TestSets {
      assert(size(set1.union(set5)) === 4)
    }
  }

  test("mostRetweeted: should return the tweet with 20 retweets") {
    new TestSets {
      assert(mostRetweetedTestSet.mostRetweeted.retweets === 20)
    }
  }

  test("mostRetweeted: Should throw exception on an empty set") {
    new TestSets {
      assertThrows[NoSuchElementException](set1.mostRetweeted)
    }
  }

  test("descending: set5") {
    new TestSets {
      val trends = set5.descendingByRetweet
      assert(!trends.isEmpty)
      assert(trends.head.user == "a" || trends.head.user == "b")
    }
  }

  test("findGoogleTweets: Should find all 6 tweets mentioning google keywords") {
    new TestSets {
      // Add some random tweets to the google set first
      val googleAndRandomTweets = googleTweets union set5
      val result = GoogleVsApple.findGoogleTweets(googleAndRandomTweets)
      assert(result contains googleTweet1)
      assert(result contains googleTweet2)
      assert(result contains googleTweet3)
      assert(result contains googleTweet4)
      assert(result contains googleTweet5)
      assert(result contains googleTweet6)
      assert(!result.contains(c))
      assert(!result.contains(d))
    }
  }

  }
