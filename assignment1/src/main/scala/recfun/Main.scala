package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }

    balance(":-)".toList)
  }

  /**
   * Exercise 1
   */
   def pascal(c: Int, r: Int): Int = {
     // End case: column is first or last column
     if (c == 0 || c == r) 1
     else {
       val prevRow = r - 1
       pascal(c - 1, prevRow) + pascal(c, prevRow)
     }
   }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {

      def balanceWithCount(remainingChars: List[Char], openParanthesesCount: Int): Boolean =
        // We are finished when no letters are remaining
        if (remainingChars.isEmpty) openParanthesesCount == 0
        else if (remainingChars.head == ')') {
          // We cannot close a paranthesis if none are currently open
          if (openParanthesesCount == 0) false
          else balanceWithCount(remainingChars.tail, openParanthesesCount - 1)
        }
        else if (remainingChars.head == '(') balanceWithCount(remainingChars.tail, openParanthesesCount + 1)
        // If the letter is not a paranthesis, just continue with the letters
        else balanceWithCount(remainingChars.tail, openParanthesesCount)

      balanceWithCount(chars, 0)
    }

  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {
      if (money == 0) 1
      else if (money < 0 || coins.isEmpty) 0
      else countChange(money - coins.head, coins) + countChange(money, coins.tail)
    }
  }
