class Rational(x: Int, y: Int) {
  def numerator = x
  def denominator = y

  def add(other: Rational) =
    new Rational(numerator * other.denominator + other.numerator * denominator,
      denominator * other.denominator)

  def substract(other: Rational) =
    new Rational(numerator * other.denominator - other.numerator * denominator,
      denominator * other.denominator)

  def negate() = new Rational(numerator * -1, denominator)

  override def toString: String = numerator + "/"  + denominator
}

val num1 = new Rational(1,2)
val num2 = new Rational(2,3)
num1.substract(num2).negate()

val x = new Rational(1,3)
val y = new Rational(5,7)
val z = new Rational(3,2)

x.substract(y).substract(z)
