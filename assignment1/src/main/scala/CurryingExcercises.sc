// A function that calculates together values applided to a function of a given interval
def reduce(reducingFunc: (Int, Int) => Int, initial: Int)(mappingFunc: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) initial
  else reducingFunc(mappingFunc(a), reduce(reducingFunc, initial)(mappingFunc)(a + 1, b))
}

/*
// A function that calculates the sum of values applided to a function of a given interval
def sum(mappingFunction: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 0
  else mappingFunction(a) + sum(mappingFunction)(a + 1, b)
}

// A function that calculates the product of values applided to a function of a given interval
def product(mappingFunction: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 1
  else mappingFunction(a) * product(mappingFunction)(a + 1, b)
}
*/

def sum(mappingFunc: Int => Int)(a: Int, b: Int): Int =
  reduce((a, b) => a + b, 0)(mappingFunc)(a,b)
def product(mappingFunc: Int => Int)(a: Int, b: Int): Int =
  reduce((a, b) => a * b, 1)(mappingFunc)(a,b)
var myFunSum = reduce((x: Int,y: Int) => x + y, 0)
product(x => x +1 )(1,3)
println("Factorial: ")
product(x => x)(1, 3)
println("Sum of 1 to 4: ")
myFunSum(x => x)(1,4)
println("2 + 1 is")
