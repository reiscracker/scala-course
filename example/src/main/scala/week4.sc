trait List[T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
}

class Nil[T] extends List[T] {
  override def isEmpty = true
  override def head = throw new NoSuchElementException("Nil.head")
  override def tail = throw new NoSuchElementException("Nil.tail")
}

class Cons[T] (val head: T, val tail: List[T]) extends List[T] {
  override def isEmpty = false
}

def nth[T](n: Int, l: List[T]) = {

  def iter(i: Int, remainingList: List[T]): T =
    if (remainingList.isEmpty) throw new IndexOutOfBoundsException
    else if (i == n) remainingList.head
    else iter(i + 1, remainingList.tail)

  iter(0, l)
}

val myList = new Cons(3, new Cons(5, new Cons(7, new Nil)))

println("The first element of myList is " + nth(0, myList))
println("The second element of myList is " + nth(1, myList))
println("The third element of myList is " + nth(2, myList))
println("The fourth element of myList is " + nth(3, myList))
